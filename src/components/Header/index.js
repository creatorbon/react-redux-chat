import React from "react";
import styles from "./styles.module.scss";

const Header = () => (
  <header className={styles.header}>
    <div className={styles.headerContainer}>
      <a href="./#">
        <i className="fas fa-comments fa-3x"></i>
        <span>React Chat</span>
      </a>
    </div>
  </header>
);

export default Header;
