import React, { useState, useEffect, useRef } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import styles from "./styles.module.scss";
import { updateMessage, cancelMessage } from "../../redux/chatAction";

const Modal = ({ isEdit, updateMessage, cancelMessage }) => {
  const [value, setValue] = useState("");

  const saveUpdateMessage = () => {
    let currentMessage = isEdit.message;
    if (value) {
      currentMessage = { ...currentMessage, text: value };
    }
    updateMessage(currentMessage);
    setValue("");
  };

  const cancelUpdateMessage = () => {
    cancelMessage();
    setValue("");
  };

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const inputRef = useRef(null);

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  }, [isEdit]);

  if (isEdit.status) {
    return (
      <div className={styles.modal}>
        <div className={styles.modalContainer}>
          <div>Edit message</div>
          <textarea
            ref={inputRef}
            value={value || isEdit.message?.text}
            onChange={handleChange}
          ></textarea>
          <button className={styles.cancel} onClick={cancelUpdateMessage}>
            Cancel
          </button>
          <button className={styles.save} onClick={saveUpdateMessage}>
            Save
          </button>
        </div>
      </div>
    );
  } else {
    return null;
  }
};

const mapStateToProps = ({ chat }) => ({
  isEdit: chat.isEdit,
});

const actions = { updateMessage, cancelMessage };

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
