import React, { useState } from "react";
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux' 
import {addMessage} from '../../redux/chatAction';
import styles from "./styles.module.scss";

const MessageInput = ({ inputRef, addMessage, user }) => {
  const [value, setValue] = useState("");

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const submitCombination = (event) => {
    if (event.ctrlKey && event.keyCode === 13 && value.trim()) {
      addMessage(value, user);
      setValue("");
    }
  };

  const submit = () => {
    if (value.trim()) {
      addMessage(value, user);
      setValue("");
    }
  };

  return (
    <div className={styles.messageInput} onKeyDown={submitCombination}>
      <textarea
        ref={inputRef}
        placeholder="Введите текст сообщения…"
        value={value}
        onChange={handleChange}
      />
      <button onClick={submit}>
        <i className="fas fa-play fa-lg"></i>
      </button>
    </div>
  );
};

const mapStateToProps = ({user}) => ({
  user: user
});

const actions = {addMessage}

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);