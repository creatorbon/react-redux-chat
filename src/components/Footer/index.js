import React from "react";
import styles from "./styles.module.scss";

const Footer = () => (
  <footer className={styles.header}>© Created by Nikita Remeslov</footer>
);

export default Footer;
