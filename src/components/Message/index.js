import React from "react";
import styles from "./styles.module.scss";
import Avatar from "../Avatar";
import moment from "moment";

const Message = ({ isMe, message, onDelete, onEdit, onLike }) => {
  const { createdAt, text, avatar } = message;
  const date = moment(createdAt).fromNow();

  const deleteMessage = () => onDelete(message.id);
  const editMessage = () => onEdit(message);
  const likeMessage = (event) => {
    if (message.likes?.length) {
      event.target.style.color = "";
    } else {
      event.target.style.color = "rgb(213, 0, 0)";
    }
    onLike(message.id);
  };

  if (isMe) {
    return (
      <div className={styles.message}>
        <div className={styles.personalMessageContainer}>
          <div className={styles.personalMessageIcons}>
            <i className="fas fa-trash-alt" onClick={deleteMessage}></i>
            <i className="fas fa-pencil-alt" onClick={editMessage}></i>
          </div>
          <div className={styles.personalMessage}>{text}</div>
        </div>
        <div className={styles.personalMessageTime}>{date}</div>
      </div>
    );
  } else {
    return (
      <>
        <div className={styles.message}>
          <Avatar avatar={avatar} />
          <div className={styles.messageContainer}>
            <div className={styles.usersMessage}>{text}</div>
            <i className="fas fa-heart liked" onClick={likeMessage}>
              {message.likes
                ? message.likes.length !== 0
                  ? message.likes.length
                  : ""
                : ""}
            </i>
          </div>
          <div className={styles.messageTime}>{date}</div>
        </div>
      </>
    );
  }
};

export default Message;
