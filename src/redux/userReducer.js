import { SET_USER } from "./actionTypes";

const initialState = {};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER:
      return {
        ...state,
        ...payload,
      };
    default:
      return state;
  }
};
