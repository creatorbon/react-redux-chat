import {
  SET_MESSAGES,
  ADD_MESSAGE,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
  SAVE_EDITED_MESSAGE,
  CANCEL_EDITED_MESSAGE,
  LIKE_MESSAGE,
} from "./actionTypes";

export const setMessage = (items) => ({
  type: SET_MESSAGES,
  payload: items,
});

export const addMessage = (message, user) => ({
  type: ADD_MESSAGE,
  payload: { message, user },
});

export const removeMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: id,
});

export const editMessage = (message) => ({
  type: EDIT_MESSAGE,
  payload: message,
});

export const updateMessage = (message) => ({
  type: SAVE_EDITED_MESSAGE,
  payload: message,
});

export const cancelMessage = () => ({
  type: CANCEL_EDITED_MESSAGE,
});

export const likeMessage = (id) => ({
  type: LIKE_MESSAGE,
  payload: id,
});
