import { v4 as uuid } from "uuid";
import moment from "moment";
import {
  SET_MESSAGES,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
  SAVE_EDITED_MESSAGE,
  CANCEL_EDITED_MESSAGE,
  ADD_MESSAGE,
  LIKE_MESSAGE,
} from "./actionTypes";

const initialState = {
  messages: [],
  isLoaded: false,
  isEdit: { status: false, message: {} },
};

export const include = (state, item) =>
  state.includes(item) ? state : [...state, item];

const getMessageIndex = (messages, id) =>
  messages.findIndex((message) => message.id === id);

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_MESSAGES:
      const sortedMessage = payload.sort(
        (message, nextMessage) =>
          moment(message.createdAt).valueOf() -
          moment(nextMessage.createdAt).valueOf()
      );
      return {
        ...state,
        messages: sortedMessage,
        isLoaded: true,
      };
    case DELETE_MESSAGE:
      return {
        ...state,
        messages: state.messages.filter((message) => message.id !== payload),
      };
    case EDIT_MESSAGE:
      return {
        ...state,
        isEdit: { status: true, message: payload },
      };
    case SAVE_EDITED_MESSAGE:
      const index = getMessageIndex(state.messages, payload.id);
      const updatedMessages = [...state.messages];
      updatedMessages[index] = payload;
      return {
        ...state,
        isEdit: { status: false, message: {} },
        messages: updatedMessages,
      };
    case CANCEL_EDITED_MESSAGE:
      return {
        ...state,
        isEdit: { status: false, message: {} },
      };
    case ADD_MESSAGE: {
      const { message, user } = payload;
      const newMessage = {
        id: uuid(),
        text: message,
        user: user.user,
        avatar: user.avatar,
        userId: user.userId,
        editedAt: "",
        createdAt: moment().format(),
      };
      return {
        ...state,
        messages: [...state.messages, newMessage],
        isLoaded: true,
      };
    }
    case LIKE_MESSAGE: {
      const index = getMessageIndex(state.messages, payload);
      const updatedMessages = [...state.messages];
      const currentMessage = updatedMessages[index];
      const currentLikes = currentMessage.likes || [];
      const userLikeIndex = currentLikes.indexOf(payload);
      if (userLikeIndex === -1) {
        updatedMessages[index] = {
          ...state.messages[index],
          likes: [...currentLikes, payload],
        };
      } else {
        currentLikes.splice(userLikeIndex, 1);
        updatedMessages[index] = {
          ...state.messages[index],
          likes: [...currentLikes],
        };
      }

      return {
        ...state,
        messages: updatedMessages,
      };
    }
    default:
      return state;
  }
};
